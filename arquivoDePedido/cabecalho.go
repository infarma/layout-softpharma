package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Cabecalho struct {
	TipoRegistro        string `json:"TipoRegistro"`
	CodigoCliente       string `json:"CodigoCliente"`
	CnpjCliente         string `json:"CnpjCliente"`
	NumeroPedidoCliente string `json:"NumeroPedidoCliente"`
	DataPedido          int32  `json:"DataPedido"`
	ReversadoSoftpharma string `json:"ReversadoSoftpharma"`
}

func (c *Cabecalho) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesCabecalho

	err = posicaoParaValor.ReturnByType(&c.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CodigoCliente, "CodigoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.CnpjCliente, "CnpjCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.DataPedido, "DataPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&c.ReversadoSoftpharma, "ReversadoSoftpharma")
	if err != nil {
		return err
	}

	return err
}

var PosicoesCabecalho = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":        {0, 1, 0},
	"CodigoCliente":       {1, 7, 0},
	"CnpjCliente":         {7, 21, 0},
	"NumeroPedidoCliente": {21, 33, 0},
	"DataPedido":          {33, 41, 0},
	"ReversadoSoftpharma": {41, 71, 0},
}
