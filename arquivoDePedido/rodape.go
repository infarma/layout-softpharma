package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Rodape struct {
	TipoRegistro            string  `json:"TipoRegistro"`
	NumeroPedidoCliente     string  `json:"NumeroPedidoCliente"`
	QuantidadeTotalItens    int32   `json:"QuantidadeTotalItens"`
	QuantidadeTotalUnidades float64 `json:"QuantidadeTotalUnidades"`
	ComplementoNumeroPedido string  `json:"ComplementoNumeroPedido"`
	ReservadoSoftpharma     string  `json:"ReservadoSoftpharma"`
}

func (r *Rodape) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRodape

	err = posicaoParaValor.ReturnByType(&r.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedidoCliente, "NumeroPedidoCliente")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalItens, "QuantidadeTotalItens")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTotalUnidades, "QuantidadeTotalUnidades")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ComplementoNumeroPedido, "ComplementoNumeroPedido")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.ReservadoSoftpharma, "ReservadoSoftpharma")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRodape = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":            {0, 1, 0},
	"NumeroPedidoCliente":     {1, 13, 0},
	"QuantidadeTotalItens":    {13, 18, 0},
	"QuantidadeTotalUnidades": {18, 28, 2},
	"ComplementoNumeroPedido": {28, 31, 0},
	"ReservadoSoftpharma":     {31, 80, 0},
}
