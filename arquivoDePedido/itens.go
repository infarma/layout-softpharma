package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Itens struct {
	TipoRegistro        string  `json:"TipoRegistro"`
	CodigoProduto       int64   `json:"CodigoProduto"`
	Quantidade          int32   `json:"Quantidade"`
	Preco               float64 `json:"Preco"`
	CodigoOferta        int32   `json:"CodigoOferta"`
	PrazoOferta         int32   `json:"PrazoOferta"`
	ReservadoSoftpharma string  `json:"ReservadoSoftpharma"`
}

func (i *Itens) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesItens

	err = posicaoParaValor.ReturnByType(&i.TipoRegistro, "TipoRegistro")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Quantidade, "Quantidade")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.Preco, "Preco")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.CodigoOferta, "CodigoOferta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.PrazoOferta, "PrazoOferta")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&i.ReservadoSoftpharma, "ReservadoSoftpharma")
	if err != nil {
		return err
	}

	return err
}

var PosicoesItens = map[string]gerador_layouts_posicoes.Posicao{
	"TipoRegistro":        {0, 1, 0},
	"CodigoProduto":       {1, 15, 0},
	"Quantidade":          {15, 20, 0},
	"Preco":               {20, 32, 2},
	"CodigoOferta":        {32, 37, 0},
	"PrazoOferta":         {37, 40, 0},
	"ReservadoSoftpharma": {40, 70, 0},
}
