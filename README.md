## Arquivo de Pedido
gerador-layouts arquivoDePedido cabecalho TipoRegistro:string:0:1 CodigoCliente:string:1:7 CnpjCliente:string:7:21 NumeroPedidoCliente:string:21:33 DataPedido:int32:33:41 ReversadoSoftpharma:string:41:71

gerador-layouts arquivoDePedido itens TipoRegistro:string:0:1 CodigoProduto:int64:1:15 Quantidade:int32:15:20 Preco:float64:20:32:2 CodigoOferta:int32:32:37 PrazoOferta:int32:37:40 ReservadoSoftpharma:string:40:70

gerador-layouts arquivoDePedido rodape TipoRegistro:string:0:1 NumeroPedidoCliente:string:1:13 QuantidadeTotalItens:int32:13:18 QuantidadeTotalUnidades:float64:18:28:2 ComplementoNumeroPedido:string:28:31 ReservadoSoftpharma:string:31:80


## Arquivo de Retorno
gerador-layouts arquivoDeRetorno cabecalho TipoRegistro:string:0:1 CodigoCliente:string:1:7 CnpjCliente:string:7:21 NumeroPedidoCliente:string:21:33 DataPedido:int32:33:41 HoraProcessamentoPedido:int32:41:49 StatusPedido:int32:49:51 ReversadoSoftpharma:string:51:81

gerador-layouts arquivoDeRetorno itens TipoRegistro:string:0:1 CodigoProduto:int64:1:15 QuantidadeAtendida:int32:15:20 QuantidadeNaoAtendida:int32:20:25 Motivo:string:25:27 ReservadoSoftpharma:string:27:57

gerador-layouts arquivoDeRetorno rodape TipoRegistro:string:0:1 NumeroPedidoCliente:string:1:13 QuantidadeTotalItens:int32:13:18 QuantidadeItensAtendidos:int32:18:23 QuantidadeItensNaoAtendidos:int32:23:28 ReservadoSoftpharma:string:28:58