package arquivoDeRetorno

type Cabecalho struct {
	TipoRegistro            string `json:"TipoRegistro"`
	CodigoCliente           string `json:"CodigoCliente"`
	CnpjCliente             string `json:"CnpjCliente"`
	NumeroPedidoCliente     string `json:"NumeroPedidoCliente"`
	DataPedido              int32  `json:"DataPedido"`
	HoraProcessamentoPedido int32  `json:"HoraProcessamentoPedido"`
	StatusPedido            int32  `json:"StatusPedido"`
	ReversadoSoftpharma     string `json:"ReversadoSoftpharma"`
}
