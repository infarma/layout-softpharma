package arquivoDeRetorno

type Itens struct {
	TipoRegistro          string `json:"TipoRegistro"`
	CodigoProduto         int64  `json:"CodigoProduto"`
	QuantidadeAtendida    int32  `json:"QuantidadeAtendida"`
	QuantidadeNaoAtendida int32  `json:"QuantidadeNaoAtendida"`
	Motivo                string `json:"Motivo"`
	ReservadoSoftpharma   string `json:"ReservadoSoftpharma"`
}
