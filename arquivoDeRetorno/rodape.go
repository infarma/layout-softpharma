package arquivoDeRetorno

type Rodape struct {
	TipoRegistro                string `json:"TipoRegistro"`
	NumeroPedidoCliente         string `json:"NumeroPedidoCliente"`
	QuantidadeTotalItens        int32  `json:"QuantidadeTotalItens"`
	QuantidadeItensAtendidos    int32  `json:"QuantidadeItensAtendidos"`
	QuantidadeItensNaoAtendidos int32  `json:"QuantidadeItensNaoAtendidos"`
	ReservadoSoftpharma         string `json:"ReservadoSoftpharma"`
}
